# -*- coding: utf-8 -*-
"""
Created on Sat Jul  6 16:12:42 2019

@author: Raviraj
"""

import pandas as pd

   
def SMA(data, n):
    
    sma = data
    sma = sma.rolling(n).mean()
    return sma


def EMA(data, n):
    
    ema = data
    ema = ema.ewm(com=(n-1), min_periods=n).mean()
    return ema


def RSI_EMA(data, n):
    
       # Get the difference in price from previous step
       delta_rsi_ema = data.diff()
       # Get rid of the first row, which is NaN since it did not have a previous
       delta_rsi_ema = delta_rsi_ema[1:]
       # Make the positive gains (up) and negative gains (down) Series
       up_rsi_ema, down_rsi_ema = delta_rsi_ema.copy(), delta_rsi_ema.copy()
       up_rsi_ema[up_rsi_ema < 0.0] = 0.0
       down_rsi_ema[down_rsi_ema > 0.0] = 0.0
       # Calculate the EWMA
       roll_up_rsi_ema = up_rsi_ema.ewm(com=(n-1), min_periods=n).mean()
       roll_down_rsi_ema = down_rsi_ema.abs().ewm(com=(n-1), min_periods=n).mean()
       # Calculate the RSI based on EWMA
       rs = roll_up_rsi_ema / roll_down_rsi_ema
       rsi_ema = 100.0 - (100.0 / (1.0 + rs))
       return rsi_ema
   
    
def RSI_SMA(data, n):
    
       # Get the difference in price from previous step
       delta_rsi_sma = data.diff()
       # Get rid of the first row, which is NaN since it did not have a previous
       delta_rsi_sma = delta_rsi_sma[1:]
       # Make the positive gains (up) and negative gains (down) Series
       up_rsi_sma, down_rsi_sma = delta_rsi_sma.copy(), delta_rsi_sma.copy()
       up_rsi_sma[up_rsi_sma < 0.0] = 0.0
       down_rsi_sma[down_rsi_sma > 0.0] = 0.0
       # Calculate the SMA
       roll_up_rsi_sma = up_rsi_sma.rolling(n).mean()
       roll_down_rsi_sma = down_rsi_sma.rolling(n).mean().abs()
       # Calculate the RSI based on SMA
       rs = roll_up_rsi_sma / roll_down_rsi_sma
       rsi_sma = 100.0 - (100.0 / (1.0 + rs))
       return rsi_sma
 

    #sample data from StockCharts
data = pd.Series( [ 814.15, 814.8, 813.55, 820.15,
                    801.2, 777.7, 775.75, 770.7,
                    771.4, 771.05, 762.85, 781.65,
                    788.6, 800.45, 808.55, 810.45,
                    802.6, 806.2, 808.85, 806.1 ] )
    
    
print (RSI_EMA (data, 14))
print (RSI_SMA(data, 14))
print (SMA (data, 5))
print (EMA (data, 5))




