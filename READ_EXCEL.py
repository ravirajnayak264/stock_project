# -*- coding: utf-8 -*-
"""
Created on Sat Jul  6 18:06:50 2019

@author: User
"""

import pandas as pd


# Create some Pandas dataframes from some data.
excel_file = 'X://STOCKDATA.xlsx'
xlsx = pd.ExcelFile(excel_file)
sheets = []
symbol=list()
close_price=list()

# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter('X://OUTPUT.xlsx', engine='xlsxwriter')

# Iterate though all Sheets
for sheet in xlsx.sheet_names:
    
# Write each dataframe to a different worksheet.
    
    contents = pd.read_excel(excel_file, sheet_name=sheet)
    symbol =list((contents['Symbol']))
    close_price =list((contents['Close Price']))

    
    
    data = {'Symbol':symbol,
        'Close Price':close_price
        } 
    
    
    df1 = pd.DataFrame(data)
    
    
    #df1 = pd.DataFrame({'Data': [111, 12, 13, 14]})
    df1.to_excel(writer,index=False, sheet_name=sheet)


# Close the Pandas Excel writer and output the Excel file.
writer.save()